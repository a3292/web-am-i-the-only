import { toRefs, usePreferredLanguages, useStorage } from "@vueuse/core";
import { computed } from "vue-demi";

const PreferredLanguages = usePreferredLanguages();

const UIStorage = useStorage("ui-state", {
  locale: PreferredLanguages.value[0].split("-")[0],
  darkMode: false,
  fingerPrintId: null
});

export function useUiState() {
  return {
    ...toRefs(UIStorage),
    shortLocale: computed(() => UIStorage.value.locale.split("_")[0])
  };
}
