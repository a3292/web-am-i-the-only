import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#0B1354",
        secondary: "#165BAA",
        accent: "#A155B9"
      },
      dark: {
        primary: "#A155B9",
        secondary: "#165BAA",
        accent: "#0B1354"
      }
    }
  }
});
