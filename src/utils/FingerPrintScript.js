import { mapValues, pick, get, values } from "lodash";

export function createFingerPrintObject(nav, screen) {
  let fingerPrintObject = {};
  fingerPrintObject = pick(nav, [
    "userAgent",
    "language",
    "languages",
    "platform",
    "deviceMemory",
    "doNotTrack",
    "cookieEnabled"
  ]);
  fingerPrintObject.doNotTrack =
    fingerPrintObject.doNotTrack == "1" || fingerPrintObject.doNotTrack == "yes"
      ? true
      : false;
  fingerPrintObject.plugins = values(
    mapValues(nav.plugins, p => {
      return {
        name: get(p, "name", ""),
        filename: get(p, "filename", ""),
        description: get(p, "description", ""),
        version: get(p, "version", "")
      };
    })
  );
  fingerPrintObject.timezone = new Date().getTimezoneOffset();
  fingerPrintObject.resolution = [
    screen.width,
    "x",
    screen.height,
    "x",
    screen.colorDepth
  ].join("");
  return fingerPrintObject;
}
